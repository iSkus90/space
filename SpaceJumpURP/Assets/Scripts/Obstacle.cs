﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    [SerializeField] float _rotation;
    [SerializeField] float _rotationSpeed;
    [SerializeField] public GameObject _VFX;
    [SerializeField] public GameObject _enemyOff;
    public Vector3 toRot;
    public Vector3 toRotDistance;
    public GameObject _pivotObject;

    [SerializeField] private bool isRotate;

    public bool _isWall;
    public bool _asteroid;
    public bool _enemy;

    private void FixedUpdate()
    {
        if (isRotate)
        {
            transform.Rotate(toRot * _rotation);
            transform.RotateAround(_pivotObject.transform.position, toRotDistance, _rotationSpeed * Time.deltaTime);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Player player = other.gameObject.GetComponent<Player>();
        if (player != null)
        {
            if (_enemy)
            {
                Player.instance.audioSource.PlayOneShot(Player.instance.audioDamage);
                _VFX.SetActive(true);
                StartCoroutine(DelayDestroy());
                _enemyOff.SetActive(false);
                player.Damage();
            }
        }
    }

    IEnumerator DelayDestroy()
    {
        yield return new WaitForSeconds(0.3f);
        Destroy(gameObject);
        Debug.Log("Удалить врага");
    }
}
